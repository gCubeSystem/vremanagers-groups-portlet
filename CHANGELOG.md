
# Changelog for VRE Managers and Groups Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.0] - 2022-11-14

- Ported to Java 11 and GWT 2.10

## [v2.3.0] - 2018-10-22

- Support #12736, VRE Managers and Groups: text mis-aligned
- Ported to Java 8 and GWT 2.8.1

## [v1.0.0] - 2014-12-09

- First release 